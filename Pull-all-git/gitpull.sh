#!/bin/bash
: '
File name: gitpull.sh
Author: Willem Visscher
Created: 25-Feb-2023
Last modified: 25-Feb-2023

Description:
The Bash script iterates through all subfolders, including nested subfolders, of the current folder 
 and executes the "git pull --all" command in any folders that have a ".git" subfolder, using a recursive function.

In the land of Git repos nested,
A Bash script was surely tested,
Through folders it crawled,
And "git pull" it called,
Leaving no repo unrested!
'


# Function to recursively iterate through subfolders
function recursive_git_pull() {
    # Iterate through all subfolders of the current folder
    for d in */ ; do
        # Check if the current subfolder contains a ".git" subfolder
        if [ -d "$d.git" ]; then
            # If it does, execute "git pull --all" command in the subfolder
            echo ""
	    echo ""
	    echo "===== Pulling for: $d ====="
	    cd "$d" && git pull --all
            # Move back to the parent folder
            cd ..
        fi
        # Recursively call the function to iterate through nested subfolders
        if [ -d "$d" ]; then
            cd "$d" && recursive_git_pull
            cd ..
        fi
    done
}

# Call the function to start the recursive iteration
recursive_git_pull
