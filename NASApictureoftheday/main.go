/*
File name: main.go
Author: Willem Visscher
Created: 31-jan-2023
Last modified: 01-Feb-2023

Description:
This is a simple python script that downloads the NASA picture of the day, saves it on disk together with the description. It will then set this picture as the current desktop.

There once was a NASA fan coder,
Who fetched daily pictures, bolder,
They'd save, change their screen,
To space scenes so keen,
A desktop to make each day's dolder!

Note: 
- This script was written for Mac, and might need some small changes if you intent to run it on Windows
- Previous pictures are not lost, they will reside in the NASA/archive subfolder, slowly filling up your disk

Limitations:
- At this time it's not possible to change the desktop picture for all virtual desktops and screens. WORKAROUND: set your background as "change every X-minutes" and point it to the "$HOME/Pictures/NASA/" folder. Latest 30min after downloading the new picture all desktops should have changed"
- When running the script multiple times in the same day, you will notice the current files also present in NASA/archive folder
- When the picture didnt change but your local date did, you might have the same picture twice on your harddrive, as the datestamp prefix change will create a new unique filename for the same picture
*/

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/mmcdole/gofeed"
)

const NASA_FEED_URL = "https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss"
const IMAGE_SAVE_DIR = "/Users/<USERNAME>/Pictures/NASA/"

func moveFilesToArchive(dir string) error {
	dir = os.ExpandEnv(dir)
	archiveDir := filepath.Join(dir, "archive")

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return fmt.Errorf("source directory does not exist: %s", dir)
	}

	if _, err := os.Stat(archiveDir); os.IsNotExist(err) {
		err = os.MkdirAll(archiveDir, os.ModePerm)
		if err != nil {
			return fmt.Errorf("error creating archive directory: %s", err)
		}
	}

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return fmt.Errorf("error reading source directory: %s", err)
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}
		srcPath := filepath.Join(dir, file.Name())
		dstPath := filepath.Join(archiveDir, file.Name())
		err = os.Rename(srcPath, dstPath)
		if err != nil {
			return fmt.Errorf("error moving file %s to archive: %s", file.Name(), err)
		}
	}

	return nil
}

func main() {
	fmt.Print("1")
	fmt.Print("2")
	fp := gofeed.NewParser()
	fmt.Print("3")
	feed, _ := fp.ParseURL(NASA_FEED_URL)
	fmt.Print("4")

	url := feed.Items[0].Enclosures[0].URL
	filename := filepath.Base(url)
	fmt.Print("5")
	today := time.Now().Format("20060102")
	newFilename := fmt.Sprintf("%s_%s", today, filename)
	savePath := filepath.Join(os.ExpandEnv(IMAGE_SAVE_DIR), newFilename)

	// create directory if it does not exist
	fmt.Print("6")
	dir := filepath.Dir(savePath)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		fmt.Print("7")
		err = os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			fmt.Printf("error creating directory: %s\n", err)
			os.Exit(1)
		}
	}
	
	err := moveFilesToArchive(IMAGE_SAVE_DIR)
	if err != nil {
		fmt.Printf("error moving files: %s\n", err)
		os.Exit(1)
	}

	// download image
	fmt.Print("8")
	client := &http.Client{Timeout: time.Second * 10}
	response, err := client.Get(url)
	if err != nil {
		fmt.Printf("error downloading image: %s\n", err)
		os.Exit(1)
	}
	defer response.Body.Close()

	imageData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Printf("error reading image data: %s\n", err)
		os.Exit(1)
	}

	err = ioutil.WriteFile(savePath, imageData, 0644)
	if err != nil {
		fmt.Printf("error writing image to disk: %s\n", err)
		os.Exit(1)
	}

	// set as desktop background
	fmt.Print("9")
	err = exec.Command("osascript", "-e", fmt.Sprintf(`tell application "Finder" to set desktop picture to POSIX file "%s"`, savePath)).Run()
	if err != nil {
		fmt.Printf("error setting desktop background: %s\n", err)
		os.Exit(1)
	}

	// write description to txt file
	re := regexp.MustCompile("<.*?>")
	title := feed.Items[0].Title
	description := re.ReplaceAllString(feed.Items[0].Description, "")
	description = strings.TrimSpace(description)
	txtFilename := strings.TrimSuffix(newFilename, filepath.Ext(newFilename)) + ".txt"
	txtSavePath := filepath.Join(os.ExpandEnv(IMAGE_SAVE_DIR), txtFilename)
	descriptionData := []byte(fmt.Sprintf("Title: %s\nDescription: %s", title, description))
	err = ioutil.WriteFile(txtSavePath, descriptionData, 0644)
	if err != nil {
		fmt.Printf("error writing description to disk: %s\n", err)
		os.Exit(1)
	}
}