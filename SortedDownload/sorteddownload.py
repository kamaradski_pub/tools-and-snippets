#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
File name: sorteddownload.py
Author: Willem Visscher
Created: 31-jan-2023
Last modified: 31-jan-2023

Description:
This is a simple python script that sorts the files in download folder into sub-folders.

A rhyme for this script, with cheer,
To help you sort files without fear,
Downloads folder all a mess,
Python's here to clean, no stress.

Documents, images, videos too,
Compressed, executables, coderel too,
In their folders, they will go,
Clean and sorted, files in tow!

Note: 
- This script was written for Mac, and might need some small changes if you intent to run it on Windows
- This script is very dumb and will not make any inteligent assumptions or decissions
"""


import os
import shutil

# Constants 
downloads_folder = os.path.expanduser("~/Downloads")
documents_folder = os.path.join(downloads_folder, "Documents")
images_folder = os.path.join(downloads_folder, "Images")
videos_folder = os.path.join(downloads_folder, "Videos")
compressed_folder = os.path.join(downloads_folder, "Compressed")
executables_folder = os.path.join(downloads_folder, "Executables")
coderelated_folder = os.path.join(downloads_folder, "Coderelated")

# list of file extensions to categorize
document_exts = [".doc", ".docx", ".txt", ".pdf", ".rtf", ".odt", ".md", ".epub", ".tex", ".wks", ".wps", ".wpd", ".key", ".odp", ".pps", ".ppt", ".pptx", ".ods", ".xlr", ".xls", ".xlsx", ".csv"]
image_exts = [".gif", ".jpg", ".jpeg", ".png", ".tif", ".tiff", ".bmp", ".svg", ".ico"]
video_exts = [".mp4", ".avi", ".flv", ".m4v", ".mov", ".wmv", ".mpeg", ".mpg", ".3gp", ".3g2", ".asf", ".rm", ".swf", ".vob", ".mkv", ".webm"]
compressed_exts = [".zip", ".rar", ".7z", ".tar", ".gz", ".xz", ".bz2", ".iso", ".dmg", ".tgz", ".xar", ".lzma", ".bzip", ".tar.gz", ".tar.bz2", ".tar.xz", ".tar.lzma"]
exe_pkg_exts = [".exe", ".apk", ".bat", ".com", ".jar", ".wsf", ".msi", ".bin", ".run", ".pkg", ".dmg", ".rpm", ".deb", ".pkg.gz", ".pkg.xz", ".pkg.bz2", ".pkg.tar.gz", ".pkg.tar.xz", ".pkg.tar.bz2", ".pkg.tar.lzma", ".apkg", ".apkg.xz", ".apkg.gz", ".apkg.bz2"]
coderel_exts = [".py", ".json", ".xml", ".go", ".bat", ".sh", ".yml", ".md", ".log"]

# create sub-folders if they don't exist
if not os.path.exists(document_folder):
    os.makedirs(document_folder)
if not os.path.exists(image_folder):
    os.makedirs(image_folder)
if not os.path.exists(video_folder):
    os.makedirs(video_folder)
if not os.path.exists(compressed_folder):
    os.makedirs(compressed_folder)
if not os.path.exists(executables_folder):
    os.makedirs(executables_folder)
if not os.path.exists(coderelated_folder):
    os.makedirs(coderelated_folder)

# sort files into sub-folders
for filename in os.listdir(downloads_folder):
    file_path = os.path.join(downloads_folder, filename)
    if os.path.isfile(file_path):
        if any(filename.endswith(ext) for ext in document_exts):
            shutil.move(file_path, document_folder)
        elif any(filename.endswith(ext) for ext in image_exts):
            shutil.move(file_path, image_folder)
        elif any(filename.endswith(ext) for ext in video_exts):
            shutil.move(file_path, video_folder)
        elif any(filename.endswith(ext) for ext in compressed_exts):
            shutil.move(file_path, compressed_folder)
        elif any(filename.endswith(ext) for ext in exe_pkg_exts):
            shutil.move(file_path, executables_folder)
        elif any(filename.endswith(ext) for ext in coderel_exts):
            shutil.move(file_path, coderelated_folder)