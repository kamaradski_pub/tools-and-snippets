/*
File name: main.go
Author: Willem Visscher
Created: 31-jan-2023
Last modified: 31-jan-2023

Description:
This is a simple Go script that sorts the files in download folder into sub-folders.

Note: 
- This script was written for Mac, and might need some small changes if you intent to run it on Windows
- This script is very dumb and will not make any inteligent assumptions or decissions
*/

package main

import (
	"os"
	"path/filepath"
)

// Constants 
var downloadsFolder = filepath.Join(os.Getenv("HOME"), "Downloads")
var documentsFolder = filepath.Join(downloadsFolder, "Documents")
var imagesFolder = filepath.Join(downloadsFolder, "Images")
var videosFolder = filepath.Join(downloadsFolder, "Videos")
var compressedFolder = filepath.Join(downloadsFolder, "Compressed")
var executablesFolder = filepath.Join(downloadsFolder, "Executables")
var coderelatedFolder = filepath.Join(downloadsFolder, "Coderelated")

// list of file extensions to categorize
var documentExts = []string{".doc", ".docx", ".txt", ".pdf", ".rtf", ".odt", ".md", ".epub", ".tex", ".wks", ".wps", ".wpd", ".key", ".odp", ".pps", ".ppt", ".pptx", ".ods", ".xlr", ".xls", ".xlsx", ".csv"}
var imageExts = []string{".gif", ".jpg", ".jpeg", ".png", ".tif", ".tiff", ".bmp", ".svg", ".ico"}
var videoExts = []string{".mp4", ".avi", ".flv", ".m4v", ".mov", ".wmv", ".mpeg", ".mpg", ".3gp", ".3g2", ".asf", ".rm", ".swf", ".vob", ".mkv", ".webm"}
var compressedExts = []string{".zip", ".rar", ".7z", ".tar", ".gz", ".xz", ".bz2", ".iso", ".dmg", ".tgz", ".xar", ".lzma", ".bzip", ".tar.gz", ".tar.bz2", ".tar.xz", ".tar.lzma"}
var exePkgExts = []string{".exe", ".apk", ".bat", ".com", ".jar", ".wsf", ".msi", ".bin", ".run", ".pkg", ".dmg", ".rpm", ".deb", ".pkg.gz", ".pkg.xz", ".pkg.bz2", ".pkg.tar.gz", ".pkg.tar.xz", ".pkg.tar.bz2", ".pkg.tar.lzma", ".apkg", ".apkg.xz", ".apkg.gz", ".apkg.bz2"}
var coderelExts = []string{".py", ".json", ".xml", ".go", ".bat", ".sh", ".yml", ".md", ".log"}

func createSubFolders() {
	folders := []string{documentFolder, imageFolder, videoFolder, compressedFolder, executablesFolder, coderelatedFolder}
	for _, folder := range folders {
		if _, err := os.Stat(folder); os.IsNotExist(err) {
			os.MkdirAll(folder, os.ModePerm)
		}
	}
}

func sortFiles() {
	filepath.Walk(downloadsFolder, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		destination := ""
		extension := filepath.Ext(path)
		switch {
		case contains(documentExts, extension):
			destination = documentFolder
		case contains(imageExts, extension):
			destination = imageFolder
		case contains(videoExts, extension):
			destination = videoFolder
		case contains(compressedExts, extension):
			destination = compressedFolder
		case contains(exePkgExts, extension):
			destination = executablesFolder
		case contains(coderelExts, extension):
			destination = coderelatedFolder
		default:
			destination = downloadsFolder
		}

		if destination != downloadsFolder {
			os.Rename(path, filepath.Join(destination, info.Name()))
		}

		return nil
	})
}

func contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

func main() {
	createSubFolders()
	sortFiles()
}




